odoo.define('pos_action_button.ActionButton', function (require){
"use strict";

var pos_screens = require('point_of_sale.screens');


var CCButton = pos_screens.ActionButtonWidget.extend({
    template: 'PymentScreen',
    button_click: function(){
        alert('Boton Presionado')
    },
});

pos_screens.define_action_button({
    'name':'Tarjeta Bancaria',
    'widget': CCButton,
    'function': function(){return this.pos;},
});

});