
# -*- coding: utf-8 -*-
{
    'name':'POS CC Payment',
    'version':'14.0.1.0',
    'summary':'Botón de pago con tarjeta bancaria en el POS',
    'category':'Point Of Sales',
    'author':'Gabriel Orosco - Cristian Paguay',
    'maintaner':'Gabriel Orosco - Cristian Paguay',
    'company':'UCE',
    'website':'http://uce.edu.ec',
    'depends':['base','point_of_sale'],
    'qweb':['static/src/view/view.xml'],
    'data':['views/view.xml'],
    'installable': True,
    'application': True,
    'auto_install': False,
    'license':'AGPL-3'
}